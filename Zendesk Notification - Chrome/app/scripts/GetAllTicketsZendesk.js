﻿$(document).ready(function () {
    var urlData = localStorage.getItem("url");
    var userdetails = localStorage.getItem("userdetails");
    var userId = localStorage.getItem("id");
    var updatedEpoch;
    var ticketUrl;
    var x;


    if (urlData != null || userdetails != null) {
        var xmlhttp = new XMLHttpRequest();
        var xmlhttp = new XMLHttpRequest();
        fullUrl = "https://" + urlData + ".zendesk.com/api/v2/users/" + userId + "/tickets/assigned.json?sort_by=updated_at&sort_order=desc"

        if (fullUrl != "") {
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var text = JSON.parse(xmlhttp.responseText);
                    myFunction(text);
                }
            };

            xmlhttp.open("GET", fullUrl, true);
            xmlhttp.setRequestHeader("Authorization", "Basic" + userdetails);
            xmlhttp.send();

            function myFunction(arr) {
                var out = "";
                var i;
                x = 0;

                for (i = 0; i < arr.tickets.length; i++) {
                    var updatedAt = new Date(arr.tickets[i].updated_at);
                    updatedEpoch = updatedAt.getTime();

                    if (updatedEpoch >= ((new Date).getTime() - 60 * 60000)) {
                        out += '<a href="https://' + urlData + '.zendesk.com/agent/tickets/' + arr.tickets[i].id + '" target="_blank"> Ticket: ' + arr.tickets[i].subject + '</a> - Updated At - ' + updatedAt + '<br />';

                        if (updatedEpoch >= ((new Date).getTime() - 1 * 60000) && arr.tickets[i].status == "open") {
                                    //Get Latest Ticket comment - Last 1 minutes
                                    var xmlhttp = new XMLHttpRequest();
                                    var xmlhttp = new XMLHttpRequest();

                                    fullUrl = 'https://' + urlData + '.zendesk.com/api/v2/tickets/' + arr.tickets[i].id + '/comments.json?sort_order=desc'

                                    if (fullUrl != "") {

                                        xmlhttp.onreadystatechange = function () {
                                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                                var text = JSON.parse(xmlhttp.responseText);
                                                myCommentFunction(text);
                                            }
                                        };

                                        xmlhttp.open("GET", fullUrl, true);
                                        xmlhttp.setRequestHeader("Authorization", "Basic Y2hyaXMud2F0c29uQGh1YmlvLmNvbTpTQE1ATnRIQDky");
                                        xmlhttp.send();

                                                function myCommentFunction(myArr) {
                                                    var i;
                                                    for (i = 0; i < 1; i++) {
                                                        var commentOutput = myArr.comments[i].body;
                                                        notifyMe('https://' + urlData + '.zendesk.com/agent/tickets/' + arr.tickets[x].id, commentOutput, arr.tickets[x].id);
                                                        console.log(commentOutput)
                                                    }
                                                }
                                            }
                                } //End Comment If statement
                    } //End IF Statement
                } 
                document.getElementById("updatedTickets").innerHTML = out;
            }
        }
    } else {
        window.location.replace("settings.html")
    }
});