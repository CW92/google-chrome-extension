﻿$(document).ready(function () {
    var urlData = localStorage.getItem("url");
    var userdetails = localStorage.getItem("userDetails");
    var userId = localStorage.getItem("id");
    var viewId = localStorage.getItem("unassignedView");
    var updatedEpoch;
    var ticketUrl;
    var x;

    if (urlData != null || userdetails != null) {

        if (viewId != null) {
            var xmlhttp = new XMLHttpRequest();
            var xmlhttp = new XMLHttpRequest();
            fullUrl = "https://" + urlData + ".zendesk.com/api/v2/views/" + viewId + "/execute.json"

            if (fullUrl != "") {
                xmlhttp.onreadystatechange = function () {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        var text = JSON.parse(xmlhttp.responseText);
                        myFunction(text);
                    }
                };

                xmlhttp.open("GET", fullUrl, true);
                xmlhttp.setRequestHeader("Authorization", userdetails);
                xmlhttp.send();

                function myFunction(arr) {
                    var out = "";
                    var i;
                    x = 0;

                    for (i = 0; i < arr.count; i++) {
                        var updatedAt = new Date(arr.rows[i].created);
                        updatedEpoch = updatedAt.getTime();

                        if (updatedEpoch >= ((new Date).getTime() - 1 * 60000)) {
                            notifyMeTicket('https://' + urlData + '.zendesk.com/agent/tickets/' + arr.rows[i].ticket_id, arr.rows[i].ticket.description, arr.rows[i].ticket_id, arr.rows[i].subject);
                        }

                        out += '<a href="https://' + urlData + '.zendesk.com/agent/filters/' + arr.rows[i].ticket_id + '" target="_blank"> Unassigned Ticket: ' + arr.rows[i].subject + '</a> - Created - ' + updatedAt + '<br />';

                    }
                    document.getElementById("newTickets").innerHTML = out;
                }
            }
        } else {
            window.location.replace("unassignedview.html?" + new Date());
        }
    };
});