﻿// request permission on page load
document.addEventListener('DOMContentLoaded', function () {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
});

function notifyMe(link, body, ticketId) {
    if (!Notification) {
        alert('Desktop notifications not available in your browser. Try Chromium.');
        return;
    }

    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification('New comment on ticket: ' + ticketId, {
            body: body,
        });

        notification.onclick = function () {
            window.open(link);
        };
    }
}

function updatedLast5Mins(value) {
    console.log(value);
    return value >= ((new Date).getTime() - 5 * 60000);
}